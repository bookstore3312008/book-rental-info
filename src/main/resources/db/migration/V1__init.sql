CREATE TABLE book
(
    isbn     VARCHAR(13) PRIMARY KEY,
    title    VARCHAR(255) NOT NULL,
    author   VARCHAR(255) NOT NULL,
    category VARCHAR(255) NOT NULL,
    borrower VARCHAR(255)
);