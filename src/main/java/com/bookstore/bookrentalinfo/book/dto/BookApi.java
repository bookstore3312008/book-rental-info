package com.bookstore.bookrentalinfo.book.dto;

import lombok.Builder;
import lombok.NonNull;
import lombok.Value;
import lombok.experimental.UtilityClass;

@UtilityClass
public class BookApi {

    @Value
    @Builder
    public static class Book {

        @NonNull
        String title;

        @NonNull
        String author;

        @NonNull
        String isbn;

        @NonNull
        String category;

        String borrower;

    }

}
