package com.bookstore.bookrentalinfo.book.infrastructure;

import com.bookstore.bookrentalinfo.book.domain.BookFacade;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@RequiredArgsConstructor
class BookInfrastructureConfiguration {

    private final BookFacade bookFacade;
    private final ObjectMapper objectMapper;

    @Bean
    BookKafkaListener bookKafkaListener() {
        return new BookKafkaListener(bookFacade, objectMapper);
    }

}
