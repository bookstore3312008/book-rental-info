package com.bookstore.bookrentalinfo.book.infrastructure;

import com.bookstore.bookrentalinfo.book.domain.BookFacade;
import com.bookstore.bookrentalinfo.book.dto.BookApi;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Set;

@RestController
@RequestMapping("/book")
@RequiredArgsConstructor
class BookController {

    private final BookFacade facade;

    @GetMapping
    Set<BookApi.Book> getRentedBooks() {
        return facade.getRentedBooks();
    }

}
