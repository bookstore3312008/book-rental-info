package com.bookstore.bookrentalinfo.book.infrastructure;

import com.bookstore.bookrentalinfo.book.dto.BookEvents;
import com.bookstore.bookrentalinfo.book.domain.BookFacade;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.messaging.handler.annotation.Header;

@Slf4j
@RequiredArgsConstructor
class BookKafkaListener {

    private static final String BOOK_RENTED_EVENT = "BookRented";
    private static final String BOOK_CREATED_EVENT = "BookCreated";

    private final BookFacade bookFacade;
    private final ObjectMapper objectMapper;

    @SneakyThrows
    @KafkaListener(topics = "book", groupId = "book-rental-info")
    void onBookRentedEvent(String event, @Header("eventType") String eventType) {
        log.info("Received event: {}", event);
        switch (eventType) {
            case BOOK_RENTED_EVENT:
                bookFacade.handleBookRentedEvent(objectMapper.readValue(event, BookEvents.BookRented.class));
                break;
            case BOOK_CREATED_EVENT:
                bookFacade.handleBookCreatedEvent(objectMapper.readValue(event, BookEvents.BookCreated.class));
                break;
            default:
                log.warn("Unknown event type: {}", eventType);
        }
    }

}


