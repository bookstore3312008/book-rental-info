package com.bookstore.bookrentalinfo.book.domain;

import com.bookstore.bookrentalinfo.book.dto.BookApi;
import lombok.RequiredArgsConstructor;

import java.util.Set;
import java.util.stream.Collectors;

@RequiredArgsConstructor
class BookQueryHandler {

    private final BookRepository repository;

    Set<BookApi.Book> getRentedBooks() {
        return repository.findByBorrowerIsNotNull()
                .stream()
                .map(Book::toApi)
                .collect(Collectors.toSet());
    }

}
