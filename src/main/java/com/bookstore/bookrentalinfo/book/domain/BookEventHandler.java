
package com.bookstore.bookrentalinfo.book.domain;

import com.bookstore.bookrentalinfo.book.dto.BookEvents;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
class BookEventHandler {

    private final BookRepository repository;

    void handleBookCreatedEvent(BookEvents.BookCreated event) {
        repository.save(Book.fromEvent(event));
    }

    void handleBookRentedEvent(BookEvents.BookRented event) {
        repository.findOrThrow(event.getIsbn()).rent(event.getClientName());
    }

}
