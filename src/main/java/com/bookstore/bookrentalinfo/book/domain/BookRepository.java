package com.bookstore.bookrentalinfo.book.domain;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.NoSuchElementException;
import java.util.Set;

interface BookRepository extends JpaRepository<Book, String> {

    Set<Book> findByBorrowerIsNotNull();

    default Book findOrThrow(String isbn) {
        return findById(isbn)
                .orElseThrow(() -> new NoSuchElementException("Book with isbn: " + isbn + " not found"));
    }

}
