package com.bookstore.bookrentalinfo.book.domain;

import com.bookstore.bookrentalinfo.book.dto.BookApi;
import com.bookstore.bookrentalinfo.book.dto.BookEvents;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

import static lombok.AccessLevel.PRIVATE;
import static lombok.AccessLevel.PROTECTED;

@Entity
@Table(name = "book")
@NoArgsConstructor(access = PROTECTED)
@AllArgsConstructor(access = PRIVATE)
class Book {

    @Id
    private String isbn;

    @NotNull
    private String title;

    @NotNull
    private String author;

    @NotNull
    private String category;

    private String borrower;

    static Book fromEvent(BookEvents.BookCreated book) {
        return new Book(
                book.getIsbn(),
                book.getTitle(),
                book.getAuthor(),
                book.getCategory(),
                book.getBorrower()
        );
    }

    void rent(String clientName) {
        borrower = clientName;
    }

    BookApi.Book toApi() {
        return BookApi.Book.builder()
                .title(title)
                .author(author)
                .isbn(isbn)
                .category(category)
                .borrower(borrower)
                .build();
    }

}
