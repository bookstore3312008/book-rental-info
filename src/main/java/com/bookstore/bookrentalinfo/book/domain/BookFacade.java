package com.bookstore.bookrentalinfo.book.domain;

import com.bookstore.bookrentalinfo.book.dto.BookApi;
import com.bookstore.bookrentalinfo.book.dto.BookEvents;
import lombok.RequiredArgsConstructor;
import org.springframework.transaction.annotation.Transactional;

import java.util.Set;

@Transactional
@RequiredArgsConstructor
public class BookFacade {

    private final BookQueryHandler queryHandler;
    private final BookEventHandler eventHandler;

    public Set<BookApi.Book> getRentedBooks() {
        return queryHandler.getRentedBooks();
    }

    public void handleBookCreatedEvent(BookEvents.BookCreated event) {
        eventHandler.handleBookCreatedEvent(event);
    }

    public void handleBookRentedEvent(BookEvents.BookRented event) {
        eventHandler.handleBookRentedEvent(event);
    }

}
