package com.bookstore.bookrentalinfo.book.domain;

import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@RequiredArgsConstructor
class BookConfiguration {

    private final BookRepository repository;

    @Bean
    BookFacade bookFacade() {
        return new BookFacade(bookQueryHandler(), bookEventHandler());
    }

    @Bean
    BookQueryHandler bookQueryHandler() {
        return new BookQueryHandler(repository);
    }

    @Bean
    BookEventHandler bookEventHandler() {
        return new BookEventHandler(repository);
    }

}
