package com.bookstore.bookrentalinfo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BookRentalInfoApplication {

    public static void main(String[] args) {
        SpringApplication.run(BookRentalInfoApplication.class, args);
    }

}